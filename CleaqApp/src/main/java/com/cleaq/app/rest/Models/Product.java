package com.cleaq.app.rest.Models;

import javax.persistence.*;

@Entity
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String name;
    @Column
    private String description;
    @Column
    private String os;
    @Column
    private String type;

    @Column
    private String ram;
    @Column
    private String process;
    @Column
    private String storage;
    @Column
    private long price;

    public long getPrice() {
        return price;
    }

    public String getProcess() {
        return process;
    }

    public String getRam() {
        return ram;
    }

    public String getStorage() {
        return storage;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public void setProcess(String process) {
        this.process = process;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public void setStorage(String storage) {
        this.storage = storage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public String getOs() {
        return os;
    }

    public String getType() {
        return type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public void setType(String type) {
        this.type = type;
    }
}
