package com.cleaq.app.rest.Controller;

import com.cleaq.app.rest.Models.Product;
import com.cleaq.app.rest.Repo.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import javax.persistence.criteria.Order;
import java.util.*;

@RestController
public class ApiControllers {

    @Autowired
    private ProductRepo ProductRepo;

    private List<Long> Panier = new ArrayList<>();

    @GetMapping(value = "/")
    public String getPage()
    {
        return "API is working";
    }

    @GetMapping(value = "/products")
    public List<Product> getProducts(){
        return ProductRepo.findAll();
    }

    @PostMapping(value = "/saveprod")
    public String saveProduct(@RequestBody Product product){
        ProductRepo.save(product);
        return "Saved...";
    }

    @GetMapping(value = "/filter")
    public Collection<Product> getFilter(
            @RequestParam Boolean IsRam,
            @RequestParam Boolean IsProcess,
            @RequestParam Boolean IsStorage,
            @RequestParam Boolean IsOS,
            @RequestParam Long MinPrice,
            @RequestParam Long MaxPrice){
        List<Sort.Order> orders = new ArrayList<Sort.Order>();
        if(IsRam)
        {
            Sort.Order order1 = new Sort.Order(Sort.Direction.DESC, "ram");
            orders.add(order1);
        }
        if(IsProcess)
        {
            Sort.Order order2 = new Sort.Order(Sort.Direction.DESC, "process");
            orders.add(order2);
        }
        if(IsStorage)
        {
            Sort.Order order3 = new Sort.Order(Sort.Direction.DESC, "storage");
            orders.add(order3);
        }
        if(IsOS)
        {
            Sort.Order order4 = new Sort.Order(Sort.Direction.DESC, "os");
            orders.add(order4);
        }
        if(IsRam)
        {
            Sort.Order order1 = new Sort.Order(Sort.Direction.DESC, "ram");
            orders.add(order1);
        }
        return ProductRepo.findAll(Sort.by(orders));
    }

    @GetMapping(value = "/panier")
    public List<Long> getPanier()
    {
        return Panier;
    }

    @PostMapping(value = "/addpanier")
    public String addPanier(@RequestParam Long id){
        Optional<Product> optionalEntity = ProductRepo.findById(id);
        if (optionalEntity.isPresent()) {
            Panier.add(id);
            return "Added...";
        }
        else{
            return "Not found";
        }
    }

    @DeleteMapping(value = "/deletevaluepanier")
    public String deleteValuePanier(@RequestParam Long id){
        Panier.remove(id);
        return "Removed...";
    }

    @GetMapping(value = "/productsnoconfig")
    public List<Product> getProductsNoConfig(){
        return ProductRepo.findProduct();
    }
}
