package com.cleaq.app.rest.Repo;

import java.util.Collection;
import java.util.List;
import com.cleaq.app.rest.Models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepo extends JpaRepository<Product, Long> {
    @Query("select u.id, u.name from Product u")
    List<Product> findProduct();
}
