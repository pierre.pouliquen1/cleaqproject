# CleaqProject

## Description 
This project is part of a desire to follow the instructions contained in the instructions.txt file.
It is therefore an API of a site offering equipment rental.

## Getting started

To use this API, clone the git repo and open it with an IDE. You are going to need to modify the file: src/main/resources/application.properties

Indeed, this file contains parameters of a database running locally.

Once done you will be able to compile the project and launch it.

## Features [W.I.P]
In order to be able to communicate with the API, I recommend that you use a framework that facilitates HTTPs requests such as postman https://www.postman.com/ .
Here are the different paths and methods you can use:

- `/` Get: to know if your API is working
- `/products` Get: to retreive all product
- `/saveprod` Post: to add a product with a body like this :
```
    {
        "name":"Mac",
        "description": "MacMagic",
        "os":"Moc",
        "type":"laptop",
        "ram":"5GO",
        "process":"5GO",
        "storage":"5GO",
        "price":5
    }
```
- `/filter` Get: to retreive product but sort by :
-- ram,
-- processor,
-- strorage
-- OS
-- in a range of MinPrice and MaxPrice (NOT WORKING YET)
For that, fill the querystring like this `?IsProcess=true?IsStorage=true?IsStorage=true?IsOs=true?MinPrice=0?MaxPrice=100`

- `/panier` Get: to retreive panier
- `/addpanier` Post: add product in panier, just add id in the query string
- `/deletevaluepanier` Delete: delete product in panier, just add id in the query string
- `/productsnoconfig`  Get: to retreive all product without configuration (NOT WORKING YET)
